result = "hole hole hole hole";

positionCellX =
[
	20,
	150,
	280,
	90,
	90,
	20,
	150,
	280
];

positionCellMuestraX = (600+50)/2;
positionCellMuestraY = 400-145;

positionCellDeckX = 400 - 115;
positionCellDeckY = (145+290)/2;

positionCellY =
[
	8,
	8,
	8,
	145,
	290,
	600 - 145,
	600 - 145,
	600 - 145
];

function preload() {

	game.load.image("1_bastos", "cartas/bastos_1.jpg");
	
}

function create() {
	
	game.stage.backgroundColor = '#00AA11';
	
	myCard = game.add.sprite(positionCellDeckX, positionCellDeckY, "1_bastos");
	
	textStyle = {font: "12px Arial"};
	textDisplay = game.add.text(0, 0, result, textStyle);

	myCard.inputEnabled = true;
	myCard.input.enableDrag();
	myCard.events.onDragStart.add(onDragStart, this);
	myCard.events.onDragStop.add(onDragSsprite.ytop, this);
	
	this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	this.scale.minWidth = 320;
	this.scale.minHeight = 480;
	this.scale.maxWidth = 768;
	this.scale.maxHeight = 1152;
  game.scale.refresh();
	
}


function onDragStart(sprite, pointer) {

    result = "Dragging " + sprite.key;
    
    

}

function onDragStop(sprite, pointer) {

    result = sprite.key + " dropped at x:" + pointer.x + " y: " + pointer.y;
    if(sprite.x > 400 - 100) sprite.x = 400 - 100;
    else if(sprite.x < 0) sprite.x = 0;
    if(sprite.y > 600 - 139) sprite.y = 600 - 139;
    else if( sprite.y < 0 ) sprite.y = 0;
    

}
